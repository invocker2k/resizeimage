const express = require('express')
const app = express()
const bodyParser = require('body-parser');
// var cors = require('cors')
// app.use(cors())

app.use(
    bodyParser.urlencoded({
      extended: true,
      limit: '100mb',
      parameterLimit: 50000,
    })
  )
  app.use(bodyParser.json({ limit: '100mb' }))

/* CROS middleware */
app.use(function(req, res, next) {
    // Mọi domain
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.listen(9999)
const routers = require('./router/')
routers(app)