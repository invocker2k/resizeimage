const reSizeController = require('../controller/ReSizeImagesController')
module.exports = app => {
        app.route('/user-photos/resize')
            .post(reSizeController.resizeImageRespondClient)
}