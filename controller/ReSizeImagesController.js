const imageDataURI = require('image-data-uri')
const sharp = require('sharp')

const resizeImageRespondClient =  async (req, res) => {
    const { data } = req.body


    const { dataBuffer, imageType } = imageDataURI.decode(data)
    if (!dataBuffer) {
      throw new Error('Lỗi khi xử lý ảnh. Vui lòng liên hệ với team code để được xử lý.')
    }
    const imageData = await sharp(dataBuffer)
      .rotate()
      .resize(1024, null, {
        fit: 'inside',
        withoutEnlargement: true,
      })
      .toBuffer()

    res.jsonp({ imageData, imageType })

}

module.exports = {
    resizeImageRespondClient,
}